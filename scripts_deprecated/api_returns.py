from typing import List
from pydantic import BaseModel

class _MemoryUsageOut(BaseModel):
    used: int
    total: int

class _NetworkUsageOut(BaseModel):
    kbytes_sent: float
    kbytes_recv: float



class SystemOut(BaseModel):
    memory_usage: _MemoryUsageOut
    cpu_usage: float
    cpu_core_usage: list
    network_usage: _NetworkUsageOut
    uptime: float

class GPUOut(BaseModel):
    gpu_id: int
    gpu_name: str
    gpu_load: float
    gpu_free_memory: int
    gpu_used_memory: int
    gpu_total_memory: int
    gpu_temperature: float
    gpu_uuid: str

class NetworkInfo(BaseModel):
    hostname: str
    port: str

class _ROS2TopicPubSubInfo(BaseModel):
    node_name: str
    node_namespace: str
    endpoint_type: str
    gid: str
    reliability: str
    durability: str
    lifespan: str
    deadline: str
    liveliness: str
    lease_duration: str


class ROS2TopicInfo(BaseModel):
    topic_name: str
    topic_type: str
    publisher_count: int
    publisher_nodes: List[_ROS2TopicPubSubInfo]
    subscriber_count: int
    subscriber_nodes: List[_ROS2TopicPubSubInfo]


class ROS2ServiceInfo(BaseModel):
    service_name: str
    service_type: str


class _ROS2NodeTopicInfo(BaseModel):
    topic_name: str
    topic_type: str

class _ROS2NodeServiceInfo(BaseModel):
    service_name: str
    service_type: str

class _ROS2NodeActionInfo(BaseModel):
    action_name: str
    action_type: str


class ROS2NodeInfo(BaseModel):
    node_name: str
    namespace: str
    subscribers: List[_ROS2NodeTopicInfo]
    publishers: List[_ROS2NodeTopicInfo]
    service_servers: List[_ROS2NodeServiceInfo]
    service_clients: List[_ROS2NodeServiceInfo]
    action_servers: List[_ROS2NodeActionInfo]
    action_clients: List[_ROS2NodeActionInfo]

class _ROS2ActionNodeInfo(BaseModel):
    node_name: str
    action_type: str

class ROS2ActionInfo(BaseModel):
    action_name: str
    action_client_count: int
    action_server_count: int
    action_client_nodes: List[_ROS2ActionNodeInfo]
    action_server_nodes: List[_ROS2ActionNodeInfo]