# ROS-tracker
[![License](https://img.shields.io/badge/License-Apache_2.0-blue.svg)](https://opensource.org/licenses/Apache-2.0)

This project collects system, network and ROS statistics and information and gives all these information with a RESTful API.

To use, just run `uvicorn app.main:app` in the main folder.

### Interface

Currently the following addresses are in use:
``` sh
/api/v0/system
/api/v0/system/gpu
/api/v0/ros2/topics
/api/v0/ros2/services
/api/v0/ros2/actions
/api/v0/ros2/nodes
/api/v0/ros2/network
/api/v0/ros2/bag
```
X is either 1 or 2.

`system` gives system usage information.

`processes` gives a list of running processes, their PIDs, and CPU usages.

`topics` gives a list of ROS topics.

`services` gives a list of ROS services.

`actions` gives a list of ROS actions.

`nodes` gives a list of ROS nodes.

`network` gives a list of network interfaces and their IP addresses.

For fast check, you can execute `curl http://127.0.0.1:8000/ros2/topics` and inspect the output to see if the server is running and functioning.

For testing, please run
```sh
python3 -m pytest
```

### Notes

`scripts` folder deprecated, and renamed to `scripts_deprecated`. That folder uses `flask` framework. If you want to try, you may run `python3 tracker.py` in the `scripts_deprecated` folder.

All code now resides in `app` folder. In `helper_programs` folder, there are independent small executables or Python libraries that are used in the API and are so small to have their own repositories. In the `versions` folder different versions of the API are stored. In these folders there are 3 main folders. `helpers`, `models` and `routes`. The `helpers` folder contains the helper functions, variables and threads that are used in the API. The `models` folder contains the request and response models that are used in the API. The `routes` folder contains the routes that are used in the API.

### To-Do
- [ ] System Info
- - [X] Total CPU usage
- - [X] Each core usage
- - [X] Memory usage
- - [X] Network usage
- - [ ] Some network statistics may be needed here?
- - [X] Uptime
- [ ] Process Info
- - [X] Process name
- - [X] Process PID
- - [ ] Process CPU usage(this is actually given, but current data can be incostistent. Needs further consideration.)
- [X] ROS2 Topics (All data that CLI gives are included here.)
- - [X] Publisher count
- - [X] Publisher nodes along with all QoS information that `ros2 topic info -v` gives(this part needs a check and feedback from network people)
- - [X] Subscriber count
- - [X] Subscriber nodes along with QoS information
- [X] ROS2 Services
- [X] ROS2 Nodes
- [X] ROS2 Actions
- [X] ROS1 Topics
- - [X] Publisher nodes and URIs(I think these URIs are special for topics and services, but need to check)
- - [X] Subscriber nodes and URIs
- [X] ROS1 Services
- - [X] Server Node
- - [X] Service Type
- - [X] Service URI
- [X] ROS1 Network
- - [X] Roscore hostname (needs to be checked)
- - [X] Roscore port
- [ ] Others
- - [ ] Latency
- - [ ] Publisher/Subscriber throughput
- - [ ] Heartbeat count
- - [X] GPU usage
- - [ ] Network info other than ROS2 gives for QoS

### Roadmap:

- [X] Tests for the currently implemented parts
- [X] ROS2 lacking parts
- [X] Storing custom topics with rosbag
- [ ] Other lacking parts
