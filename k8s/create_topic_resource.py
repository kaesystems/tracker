import os
from pprint import pprint

from kubernetes import client, config

def main():
    
    try:
        # if running in cluster with service account
        config.load_incluster_config()
    except:
        # if it can be loaded from KUBECONFIG environemnt variable
        config.load_kube_config()

    api = client.CustomObjectsApi()

    TOPIC_RESOURCE_GROUP = os.getenv("K8S_TOPIC_RESOURCE_GROUP", default="robot.roboscale.io")
    TOPIC_RESOURCE_VERSION = os.getenv("K8S_TOPIC_RESOURCE_VERSION", default="v1alpha1")
    TOPIC_RESOURCE_API_VERSION = TOPIC_RESOURCE_GROUP + "/" + TOPIC_RESOURCE_VERSION
    TOPIC_RESOURCE_KIND = os.getenv('K8S_TOPIC_RESOURCE_KIND', default="RobotTopic")
    TOPIC_RESOURCE_NAME = os.getenv('K8S_TOPIC_RESOURCE_NAME', default="example") # never use default value
    NAMESPACE = os.getenv('K8S_NAMESPACE', default="ns-1") # never use default value

    # RobotTopics resource named `example``
    my_resource = {
        "apiVersion": TOPIC_RESOURCE_API_VERSION,
        "kind": TOPIC_RESOURCE_KIND,
        "metadata": {
            "name": TOPIC_RESOURCE_NAME, 
            "namespace": NAMESPACE,
        },
        "data": {
            "message": "Topics received from ROS Python library",
            "topicList": [
                {
                    "name": "topic_name",
                    "type": ["type1", "type2"],
                    "publisherCount": 2,
                    "publisherNodes": [
                        {
                            "nodeName": "publisher1",
                            "nodeNamespace": "publisher1_ns",
                        },
                        {
                            "nodeName": "publisher2",
                            "nodeNamespace": "publisher2_ns",
                        },
                    ],
                    "subscriberCount": 2,
                    "subscriberNodes": [
                        {
                            "nodeName": "subscriber1",
                            "nodeNamespace": "subscriber1_ns",
                        },
                        {
                            "nodeName": "subscriber2",
                            "nodeNamespace": "subscriber2_ns",
                        },
                    ],
                },
            ],
        },
    }

     # create the resource RobotTopics
    try:
        api.create_namespaced_custom_object(
            group="robot.roboscale.io",
            version="v1alpha1",
            namespace="ns-1",
            plural="robottopics",
            body=my_resource,
        )

        print("Resource is created successfuly.")
    except Exception as e:
        print("Cannot create resource.")
        print(str(e))

if __name__ == "__main__":
    main()
