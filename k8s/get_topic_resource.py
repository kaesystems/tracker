import os
from pprint import pprint

from kubernetes import client, config

def main():
    
    try:
        # if running in cluster with service account
        config.load_incluster_config()
    except:
        # if it can be loaded from KUBECONFIG variable
        config.load_kube_config()

    api = client.CustomObjectsApi()

    TOPIC_RESOURCE_GROUP = os.getenv("K8S_TOPIC_RESOURCE_GROUP", default="robot.roboscale.io")
    TOPIC_RESOURCE_VERSION = os.getenv("K8S_TOPIC_RESOURCE_VERSION", default="v1alpha1")
    TOPIC_RESOURCE_KIND = os.getenv('K8S_TOPIC_RESOURCE_KIND', default="RobotTopic")
    TOPIC_RESOURCE_KIND_PLURAL = TOPIC_RESOURCE_KIND.lower() + "s"
    TOPIC_RESOURCE_NAME = os.getenv('K8S_TOPIC_RESOURCE_NAME', default="example") # never use default value
    NAMESPACE = os.getenv('K8S_NAMESPACE', default="ns-1") # never use default value

    # get the resource and print out data
    try:
        resource = api.get_namespaced_custom_object(
            group=TOPIC_RESOURCE_GROUP,
            version=TOPIC_RESOURCE_VERSION,
            name= TOPIC_RESOURCE_NAME,
            namespace=NAMESPACE,
            plural=TOPIC_RESOURCE_KIND_PLURAL,
        )

        print("Resource is fetched successfully:")
        print("Resource details:")
        pprint(resource)
    except Exception as e:
        print(str(e))    

if __name__ == "__main__":
    main()
