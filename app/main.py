from fastapi import FastAPI

from .versions.v0 import main as main_v0

app = FastAPI(
    title="ROS Tracker API",
    description="Backend service for robot tracking and ROS handling.",
    version="0.0.1"
)

app.include_router(
    main_v0.router,
    prefix="/api/v0",
)

@app.get("/")
async def root():
    return {
        "status": "SUCCESS",
        "message": "Welcome, It's ROS Tracker",
        "quote": {
            "quote": "If I had asked people what they wanted, they would have said faster horses.",
            "owner": "Henry Ford"
        } 
    }