from fastapi import APIRouter

from ..helpers import globals
from ..models import system

def _checkForErrors():
    if globals.system_info_error == globals.CannotUsePsutilError:
        return {"status": "Cannot get system information.", "info": []}
    elif globals.system_info_error == globals.CannotReachGPUError:
        return {"status": "Cannot get GPU information.", "info": []}
    else:
        return None


router = APIRouter()

@router.get("/", response_model = system.SystemResult)
async def system_info():
    error = _checkForErrors()
    if error is not None:
        return error

    with globals.general_lock:
        output = {"status": "success", "info": {"memory_usage": globals.memory_usage, "cpu_usage": globals.cpu_usage, "cpu_core_usage": globals.cpu_core_usage ,"network_usage": globals.network_usage, "uptime": globals.uptime}}
    
    return output

@router.get("/gpu", response_model = system.GPUResult)
async def gpu():
    error = _checkForErrors()
    if error is not None:
        return error

    with globals.general_lock:
        output = globals.gpu_info
    
    return {"status": "success", "info": output}