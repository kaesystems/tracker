from fastapi import APIRouter

from ..helpers import globals
from ..models import ros2


def _checkForErrors():
    if globals.ros_info_error == globals.NoROScoreError:
        return {"status": "The communication with ROS is broken.", "info": []}
    elif globals.ros_info_error == globals.CannotParseError:
        return {"status": "ROS2 CLI output is corrupted.", "info": []}
    else:
        return None


router = APIRouter()

@router.put("/bag")
async def ros2Bag(command: str, bag_name: str, topic_name: str):
    if command == "start":
        globals.rosbag_lock.acquire()
        globals.rosbag_start_list.append({"topic_name": topic_name, "bag_name": bag_name})
        globals.rosbag_lock.release()
    elif command == "stop":
        globals.rosbag_lock.acquire()
        globals.rosbag_close_list.append({"bag_name": bag_name})
        globals.rosbag_lock.release()

@router.get("/topics", response_model = ros2.ROS2TopicResult)
async def ros2Topics():
    error = _checkForErrors()
    if error is not None:
        return error

    globals.general_lock.acquire()
    output = globals.topics
    globals.general_lock.release()
    return ros2.ROS2TopicResult(
        message = "success",
        topicList = output,
        lastUpdateTimestamp=globals.timestamp
        )
    
@router.get("/services", response_model = ros2.ROS2ServiceResult)
async def ros2Services():
    error = _checkForErrors()
    if error is not None:
        return error

    globals.general_lock.acquire()
    output = globals.services
    globals.general_lock.release()
    return ros2.ROS2ServiceResult(
        message = "success",
        serviceList = output,
        lastUpdateTimestamp=globals.timestamp
        )
    
@router.get("/nodes", response_model = ros2.ROS2NodeResult)
async def ros2Nodes():
    error = _checkForErrors()
    if error is not None:
        return error

    globals.general_lock.acquire()
    output = globals.nodes
    globals.general_lock.release()
    return ros2.ROS2NodeResult(
        message = "success",
        nodeList = output,
        lastUpdateTimestamp=globals.timestamp
        )
    
@router.get("/actions", response_model = ros2.ROS2ActionResult)
async def ros2Actions():
    error = _checkForErrors()
    if error is not None:
        return error

    globals.general_lock.acquire()
    output = globals.actions
    globals.general_lock.release()
    return ros2.ROS2ActionResult(
        message = "success",
        actionList = output,
        lastUpdateTimestamp=globals.timestamp
        )
    