from threading import Thread
import time
from fastapi import APIRouter

from . import ros2, system
from ..helpers import ros_info_collector, system_info_collector, rosbag_handler, globals
from ..sync import update as k8s

router = APIRouter()

router.include_router(
    ros2.router,
    prefix="/ros2",
    #responses={418: {"description": "I'm a teapot"}},
)

router.include_router(
    system.router,
    prefix="/system",
)

def informationUpdaters():
    node = None
    if globals.ROS_VERSION == 2:
        node = ros_info_collector.startNode()
    
    while True:
        now = time.time()
        system_info_collector.systemThreadHelper()
        rosbag_handler.rosBagHandlerHelper()

        if node != None:
            ros_info_collector.rosThreadHelper(node)
        
        k8s.update_kubernetes_resources()
        
        elapsed = time.time() - now
        time.sleep(globals.LOOP_TIME - elapsed)

@router.on_event("startup")
async def startup_event():
    Thread(target=informationUpdaters).start()
