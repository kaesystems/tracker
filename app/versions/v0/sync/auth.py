import os
from kubernetes import client, config
from pydantic import BaseModel
from . import models

class K8SConfig(BaseModel):
    cli: client.CustomObjectsApi
    env: models.K8SEnv

    class Config:
        arbitrary_types_allowed = True

def get_kubernetes_config() -> K8SConfig:
    return K8SConfig(
        cli=get_api_client(),
        env=get_environment_variables(),
    )

def get_api_client():    
    # if running in cluster with service account
    config.load_incluster_config()
    # if it can be loaded from KUBECONFIG variable
    # config.load_kube_config()
    
    return client.CustomObjectsApi()

def get_environment_variables() -> models.K8SEnv:
    return models.K8SEnv(
        namespace=os.getenv(models.NAMESPACE_KEY),
        resource_group=os.getenv(models.RESOURCE_GROUP_KEY, default="robot.roboscale.io"),
        resource_version=os.getenv(models.RESOURCE_VERSION_KEY, default="v1alpha1"),
        node_env=models.EnvPerResource(
            resource_kind=os.getenv(models.NODE_RESOURCE_KIND_KEY, default="RobotNode"),
            resource_name=os.getenv(models.NODE_RESOURCE_NAME_KEY)
        ),
        topic_env=models.EnvPerResource(
            resource_kind=os.getenv(models.TOPIC_RESOURCE_KIND_KEY, default="RobotTopic"),
            resource_name=os.getenv(models.TOPIC_RESOURCE_NAME_KEY)
        ),
        service_env=models.EnvPerResource(
            resource_kind=os.getenv(models.SERVICE_RESOURCE_KIND_KEY, default="RobotService"),
            resource_name=os.getenv(models.SERVICE_RESOURCE_NAME_KEY)
        ),
        action_env=models.EnvPerResource(
            resource_kind=os.getenv(models.ACTION_RESOURCE_KIND_KEY, default="RobotAction"),
            resource_name=os.getenv(models.ACTION_RESOURCE_NAME_KEY)
        ),
    )