from . import auth
from ..models import ros2

def sync_nodes(node_result: ros2.ROS2NodeResult) -> bool:
    try:
        config = auth.get_kubernetes_config()
        api = config.cli
        k8s_keys = config.env
    except Exception as e:
        # LOG ME PLEASE !
        # error at fetching kubernetes auth or environment variables
        print(str(e))
        return False

    patch_body = {
        "data": node_result.dict(by_alias=True),
    }

    try:
        api.patch_namespaced_custom_object(
            group=k8s_keys.resource_group,
            version=k8s_keys.resource_version,
            name=k8s_keys.node_env.resource_name,
            namespace=k8s_keys.namespace,
            plural=k8s_keys.node_env.resource_kind.lower() + "s",
            body=patch_body,
        )

        return True
    except Exception as e:
        # LOG ME PLEASE !
        # cannot update nodes
        print(str(e))
        return False

def sync_topics(topic_result: ros2.ROS2TopicResult) -> bool:
    try:
        config = auth.get_kubernetes_config()
        api = config.cli
        k8s_keys = config.env
    except Exception as e:
        # LOG ME PLEASE !
        # error at fetching kubernetes auth or environment variables
        print(str(e))
        return False

    patch_body = {
        "data": topic_result.dict(by_alias=True),
    }

    try:
        api.patch_namespaced_custom_object(
            group=k8s_keys.resource_group,
            version=k8s_keys.resource_version,
            name=k8s_keys.topic_env.resource_name,
            namespace=k8s_keys.namespace,
            plural=k8s_keys.topic_env.resource_kind.lower() + "s",
            body=patch_body,
        )

        return True
    except Exception as e:
        # LOG ME PLEASE !
        # cannot update topics
        print(str(e))
        return False

def sync_services(service_result: ros2.ROS2ServiceResult) -> bool:
    try:
        config = auth.get_kubernetes_config()
        api = config.cli
        k8s_keys = config.env
    except Exception as e:
        # LOG ME PLEASE !
        # error at fetching kubernetes auth or environment variables
        print(str(e))
        return False

    patch_body = {
        "data": service_result.dict(by_alias=True),
    }

    try:
        api.patch_namespaced_custom_object(
            group=k8s_keys.resource_group,
            version=k8s_keys.resource_version,
            name=k8s_keys.service_env.resource_name,
            namespace=k8s_keys.namespace,
            plural=k8s_keys.service_env.resource_kind.lower() + "s",
            body=patch_body,
        )

        return True
    except Exception as e:
        # LOG ME PLEASE !
        # cannot update services
        print(str(e))
        return False

def sync_actions(action_result: ros2.ROS2ActionResult) -> bool:
    try:
        config = auth.get_kubernetes_config()
        api = config.cli
        k8s_keys = config.env
    except Exception as e:
        # LOG ME PLEASE !
        # error at fetching kubernetes auth or environment variables
        print(str(e))
        return False

    patch_body = {
        "data": action_result.dict(by_alias=True),
    }

    try:
        api.patch_namespaced_custom_object(
            group=k8s_keys.resource_group,
            version=k8s_keys.resource_version,
            name=k8s_keys.action_env.resource_name,
            namespace=k8s_keys.namespace,
            plural=k8s_keys.action_env.resource_kind.lower() + "s",
            body=patch_body,
        )

        return True
    except Exception as e:
        # LOG ME PLEASE !
        # cannot update actions
        print(str(e))
        return False