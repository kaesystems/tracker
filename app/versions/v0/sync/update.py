import datetime, os
from pydantic import BaseModel
from . import sync
from . import models as sync_models
from ..models import ros2
from ..helpers import globals

class Frequencies(BaseModel):
    nodes: str
    topics: str
    services: str
    actions: str

def get_frequencies() -> Frequencies:
    return Frequencies(
        nodes=os.getenv(sync_models.NODE_FREQUENCY, default="0"),
        topics=os.getenv(sync_models.TOPIC_FREQUENCY, default="0"),
        services=os.getenv(sync_models.SERVICE_FREQUENCY, default="0"),
        actions=os.getenv(sync_models.ACTION_FREQUENCY, default="0"),
    )

def update_kubernetes_resources():
    
    frequencies = get_frequencies()
    timestamp = datetime.datetime.utcnow().isoformat("T") + "Z"

    if frequencies.nodes != "0":
        if not sync.sync_nodes(node_result=ros2.ROS2NodeResult(
            message="nodes are updated",
            lastUpdateTimestamp=timestamp,
            nodeList=globals.nodes,
        )):
            raise Exception("k8s: error when updating node info")
    
    if frequencies.topics != "0":
        if not sync.sync_topics(topic_result=ros2.ROS2TopicResult(
            message="topics are updated",
            lastUpdateTimestamp=timestamp,
            topicList=globals.topics,
        )):
            raise Exception("k8s: error when updating topic info")

    if frequencies.services != "0":
        if not sync.sync_services(service_result=ros2.ROS2ServiceResult(
            message="services are updated",
            lastUpdateTimestamp=timestamp,
            serviceList=globals.services,
        )):
            raise Exception("k8s: error when updating service info")

    if frequencies.actions != "0":
        if not sync.sync_actions(action_result=ros2.ROS2ActionResult(
            message="actions are updated",
            lastUpdateTimestamp=timestamp,
            actionList=globals.actions,
        )):
            raise Exception("k8s: error when updating action info")