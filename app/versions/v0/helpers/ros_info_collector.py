import sys

from . import globals
from datetime import datetime

if globals.ROS_VERSION == 1:
    from .ros1_info_parser import ROS1 as ROS
elif globals.ROS_VERSION == 2:
    import rclpy
    if globals.USE_CLI:
        from .ros2_info_parser import ROS2CLI as ROS
    else:
        from .ros2_rclpy import ROS2 as ROS

def startNode():
    node = None
    if not globals.USE_CLI:
        rclpy.init(args=sys.argv)
        node = rclpy.create_node('ros_info_collector')

    return node

def rosThreadHelper(node):
    try:
        temp_topics = ROS.getTopics(node)
        temp_nodes = ROS.getNodes(node)
        temp_services = ROS.getServices(node)
        temp_actions = ROS.getActions(node)
        globals.timestamp = datetime.utcnow().isoformat("T") + "Z"
    except (globals.NoROScoreError, globals.CannotParseError) as e:
        globals.ros_info_error = e
    else:
        globals.ros_info_error = None

        with globals.general_lock:
            globals.topics = temp_topics
            globals.nodes = temp_nodes
            globals.services = temp_services
            globals.actions = temp_actions

