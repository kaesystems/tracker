import psutil
import GPUtil
import time

# This function returns the memory used in the system in kilobytes
def getMemoryUsage():
    # get the memory usage
    memory_usage = psutil.virtual_memory()
    # return the dictionary
    return {'used': memory_usage.used/1024, 'total': memory_usage.total/1024}

# Returns all processes and their CPU usages in a list
# This function takes too much time, because while taking CPU usages, we get an average usage of "0.01" seconds for each process.
# This must be executed in a separate thread.
def getProcessCpuUsage():
    process_cpu_usage = []
    for proc in psutil.process_iter():
        try:
            # Get process name & pid from process object.
            processName = proc.name()
            processID = proc.pid

            process_cpu_usage.append([processName, processID, proc.cpu_percent(interval=0.01)])
        except (psutil.NoSuchProcess, psutil.AccessDenied, psutil.ZombieProcess):
            pass
    return process_cpu_usage

# This function returns the total CPU usage of the system
def getTotalCpuUsage():
    # this returns 0 in every first call, I don't know the reason
    total_cpu_usage = psutil.cpu_percent()
    core_cpu_usage = psutil.cpu_percent(percpu=True)
    return total_cpu_usage, core_cpu_usage

# This function returns the network usage of the system as a dictionary in kilobytes
# This can be used to get the error rate or drop rate in the network in future!
def getNetworkUsageDict():
    # get the network usage
    network_usage = psutil.net_io_counters()
    # return the dictionary
    return {'kbytes_sent': network_usage.bytes_sent/1024, 'kbytes_recv': network_usage.bytes_recv/1024}

def getUptime():
    return time.time() - psutil.boot_time()

def getGPUInfo():
    try:
        gpus = GPUtil.getGPUs()
    except:
        return [{"gpu_id": -1, 
                "gpu_name": "Unknown", 
                "gpu_load": 0, 
                "gpu_free_memory": 0, 
                "gpu_used_memory": 0, 
                "gpu_total_memory": 0, 
                "gpu_temperature": 0, 
                "gpu_uuid": "Unknown"}]
    gpu_info = []
    for gpu in gpus:
        # get the GPU id
        gpu_id = gpu.id
        # name of GPU
        gpu_name = gpu.name
        # get % percentage of GPU usage of that GPU
        gpu_load = f"{gpu.load*100}%"
        # get free memory in MB format
        gpu_free_memory = f"{gpu.memoryFree}MB"
        # get used memory
        gpu_used_memory = f"{gpu.memoryUsed}MB"
        # get total memory
        gpu_total_memory = f"{gpu.memoryTotal}MB"
        # get GPU temperature in Celsius
        gpu_temperature = f"{gpu.temperature} °C"
        gpu_uuid = gpu.uuid
        gpu_info.append({"gpu_id": gpu_id, 
                        "gpu_name": gpu_name, 
                        "gpu_load": gpu_load, 
                        "gpu_free_memory": gpu_free_memory, 
                        "gpu_used_memory": gpu_used_memory, 
                        "gpu_total_memory": gpu_total_memory, 
                        "gpu_temperature": gpu_temperature, 
                        "gpu_uuid": gpu_uuid})
    return gpu_info