from .globals import NoROScoreError, CannotParseError

import rclpy.action

# I am assuming that in one topic there will not be more than one type
# BUT ROS2 ACTUALLY ALLOWS THIS!

class ROS2:

    @staticmethod
    def getTopics(node = None):
        topics = node.get_topic_names_and_types()
        return_list = []
        for topic in topics:
            topic_name = topic[0]
            topic_types = topic[1]
            topic_publishers = node.get_publishers_info_by_topic(topic_name)
            topic_subscribers = node.get_subscriptions_info_by_topic(topic_name)
            topic_info = {
                "topic_name": topic_name,
                "topic_type": topic_types,
                "publisher_count": len(topic_publishers),
                "publisher_nodes": [{"node_name": publisher.node_name, "node_namespace": publisher.node_namespace} for publisher in topic_publishers],
                "subscriber_count": len(topic_subscribers),
                "subscriber_nodes": [{"node_name": subscriber.node_name, "node_namespace": subscriber.node_namespace} for subscriber in topic_subscribers]
            }
            return_list.append(topic_info)
        return return_list

    @staticmethod
    def getNodes(node = None):
        nodes = node.get_node_names_and_namespaces()
        return_list = []
        for n in nodes:
            node_name = n[0]
            node_namespace = n[1]
            node_subscribers = node.get_subscriber_names_and_types_by_node(node_name, node_namespace)
            node_publishers = node.get_publisher_names_and_types_by_node(node_name, node_namespace)
            node_services = node.get_service_names_and_types_by_node(node_name, node_namespace)
            node_actions = rclpy.action.get_action_server_names_and_types_by_node(node, node_name, node_namespace)
            node_info = {
                "node_name": node_name,
                "namespace": node_namespace,
                "subscribers": [{"name": subscriber[0], "type": subscriber[1]} for subscriber in node_subscribers],
                "publishers": [{"name": publisher[0], "type": publisher[1]} for publisher in node_publishers],
                "service_servers": [{"name": service[0], "type": service[1]} for service in node_services],
                "service_clients": [{"name": service[0], "type": service[1]} for service in node_services],
                "action_servers": [{"name": action[0], "type": action[1]} for action in node_actions],
                "action_clients": [{"name": action[0], "type": action[1]} for action in node_actions]
            }
            return_list.append(node_info)
        return return_list

    @staticmethod
    def getServices(node = None):
        services = node.get_service_names_and_types()
        return_list = []
        for service in services:
            service_name = service[0]
            service_types = service[1]
            service_info = {
                "name": service_name,
                "type": service_types,
            }
            return_list.append(service_info)
        return return_list

    @staticmethod
    def getActions(node = None):
        actions = rclpy.action.get_action_server_names_and_types_by_node(node, node.get_name(), node.get_namespace())
        return_list = []
        for action in actions:
            action_name = action[0]
            action_types = action[1]
            action_info = {
                "action_name": action_name,
                "action_types": action_types,
                "action_client_count": len(action[2]),
                "action_server_count": len(action[3]),
                "action_client_nodes": action[2],
                "action_server_nodes": action[3],
            }
            return_list.append(action_info)
        return return_list
