from . import system_info_helpers, globals


def systemThreadHelper():
    try:
        temp_memory_usage = system_info_helpers.getMemoryUsage()
        temp_cpu_usage, temp_core_usage = system_info_helpers.getTotalCpuUsage()
        temp_network_usage = system_info_helpers.getNetworkUsageDict()
        temp_uptime = system_info_helpers.getUptime()
        temp_gpu_info = system_info_helpers.getGPUInfo()
    except globals.CannotUsePsutilError:
        globals.system_info_error = globals.CannotUsePsutilError
    except globals.CannotReachGPUError:
        globals.system_info_error = globals.CannotReachGPUError
    else:
        globals.system_info_error = None
        with globals.general_lock:
            globals.memory_usage = temp_memory_usage
            globals.cpu_usage = temp_cpu_usage
            globals.cpu_core_usage = temp_core_usage
            globals.network_usage = temp_network_usage
            globals.uptime = temp_uptime
            globals.gpu_info = temp_gpu_info
