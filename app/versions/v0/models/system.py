from pydantic import BaseModel
from typing import List

class _MemoryUsageOut(BaseModel):
    used: int
    total: int

class _NetworkUsageOut(BaseModel):
    kbytes_sent: float
    kbytes_recv: float

class _SystemInfo(BaseModel):
    memory_usage: _MemoryUsageOut
    cpu_usage: float
    cpu_core_usage: list
    network_usage: _NetworkUsageOut
    uptime: float

class _GPUInfo(BaseModel):
    gpu_id: int
    gpu_name: str
    gpu_load: str
    gpu_free_memory: str
    gpu_used_memory: str
    gpu_total_memory: str
    gpu_temperature: str
    gpu_uuid: str


class SystemResult(BaseModel):
    status: str
    info: _SystemInfo

class GPUResult(BaseModel):
    status: str
    info: List[_GPUInfo]

