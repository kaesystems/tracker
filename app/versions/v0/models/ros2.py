from typing import List
from pydantic import BaseModel, Field

class _ROS2TopicPubSubInfo(BaseModel):
    node_name: str = Field(..., alias="nodeName")
    node_namespace: str = Field(..., alias="nodeNamespace")

    class Config:
        allow_population_by_field_name = True

class _ROS2ActionNodeInfo(BaseModel):
    node_name: str = Field(..., alias="name")
    action_type: str = Field(..., alias="type")

    class Config:
        allow_population_by_field_name = True

class _ROS2TopicInfo(BaseModel):
    topic_name: str = Field(..., alias="name")
    topic_type: List[str] = Field(..., alias="type")
    publisher_count: int = Field(..., alias="publisherCount")
    publisher_nodes: List[_ROS2TopicPubSubInfo] = Field(..., alias="publisherNodes")
    subscriber_count: int = Field(..., alias="subscriberCount")
    subscriber_nodes: List[_ROS2TopicPubSubInfo] = Field(..., alias="subscriberNodes")

    class Config:
        allow_population_by_field_name = True

class _NameTypeInfo(BaseModel):
    name: str = Field(..., alias="name")
    type: List[str] = Field(..., alias="type")

    class Config:
        allow_population_by_field_name = True

class _ROS2NodeInfo(BaseModel):
    node_name: str = Field(..., alias="name")
    namespace: str = Field(..., alias="namespace")
    subscribers: List[_NameTypeInfo] = Field(..., alias="subscribers")
    publishers: List[_NameTypeInfo] = Field(..., alias="publishers")
    service_servers: List[_NameTypeInfo] = Field(..., alias="serviceServers")
    service_clients: List[_NameTypeInfo] = Field(..., alias="serviceClients")
    action_servers: List[_NameTypeInfo] = Field(..., alias="actionServers")
    action_clients: List[_NameTypeInfo] = Field(..., alias="actionClients")

    class Config:
        allow_population_by_field_name = True

class _ROS2ActionInfo(BaseModel):
    action_name: str = Field(..., alias="name")
    action_types: List[str] = Field(..., alias="type")
    action_client_count: int = Field(..., alias="clientCount")
    action_server_count: int = Field(..., alias="serverCount")
    action_client_nodes: List[_ROS2ActionNodeInfo] = Field(..., alias="clientNodes")
    action_server_nodes: List[_ROS2ActionNodeInfo] = Field(..., alias="serverNodes")

    class Config:
        allow_population_by_field_name = True

class ROS2TopicResult(BaseModel):
    status: str = Field(..., alias="message")
    last_update_timestamp: str = Field(..., alias="lastUpdateTimestamp")
    info: List[_ROS2TopicInfo] = Field(..., alias="topicList")

    class Config:
        allow_population_by_field_name = True

class ROS2ServiceResult(BaseModel):
    status: str = Field(..., alias="message")
    last_update_timestamp: str = Field(..., alias="lastUpdateTimestamp")
    info: List[_NameTypeInfo] = Field(..., alias="serviceList")

    class Config:
        allow_population_by_field_name = True

class ROS2NodeResult(BaseModel):
    status: str = Field(..., alias="message")
    last_update_timestamp: str = Field(..., alias="lastUpdateTimestamp")
    info: List[_ROS2NodeInfo] = Field(..., alias="nodeList")

    class Config:
        allow_population_by_field_name = True

class ROS2ActionResult(BaseModel):
    status: str = Field(..., alias="message")
    last_update_timestamp: str = Field(..., alias="lastUpdateTimestamp")
    info: List[_ROS2ActionInfo] = Field(..., alias="actionList")

    class Config:
        allow_population_by_field_name = True
